#include <iostream>
#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/block_cipher.h>
#include <botan/rng.h>
#include <botan/rsa.h>
#include<botan/system_rng.h>
using namespace std;
using namespace Botan;

enum class HashFunctionChoice{SHA3_256, SHA3_512};

//  ####################################################################################################################
//  ################################                 SANDBOX                 ###########################################
//  ####################################################################################################################


void hash_example() {
    std::unique_ptr<Botan::HashFunction> hash1(Botan::HashFunction::create("SHA-256"));
    std::unique_ptr<Botan::HashFunction> hash2(Botan::HashFunction::create("SHA-384"));
    std::unique_ptr<Botan::HashFunction> hash3(Botan::HashFunction::create("SHA-3"));
    std::vector<uint8_t> buf(2048);

    //read STDIN to buffer
    std::cin.read(reinterpret_cast<char*>(buf.data()), buf.size());
    size_t readcount = std::cin.gcount();
    //update hash computations with read data
    hash1->update(buf.data(),readcount);
    hash2->update(buf.data(),readcount);
    hash3->update(buf.data(),readcount);

    std::cout << "SHA-256: " << Botan::hex_encode(hash1->final()) << std::endl;
    std::cout << "SHA-384: " << Botan::hex_encode(hash2->final()) << std::endl;
    std::cout << "SHA-3: " << Botan::hex_encode(hash3->final()) << std::endl;
}

void aes_example() {
    std::vector<uint8_t> key = Botan::hex_decode("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F");
    std::vector<uint8_t> block = Botan::hex_decode("00112233445566778899AABBCCDDEEFF");
    std::unique_ptr<Botan::BlockCipher> cipher(Botan::BlockCipher::create("AES-256"));
    cipher->set_key(key);
    cipher->encrypt(block);
    std::cout << std::endl <<cipher->name() << "single block encrypt: " << Botan::hex_encode(block);

    //clear cipher for 2nd encryption with other key
    cipher->clear();
    key = Botan::hex_decode("1337133713371337133713371337133713371337133713371337133713371337");
    cipher->set_key(key);
    cipher->encrypt(block);

    std::cout << std::endl << cipher->name() << "single block encrypt: " << Botan::hex_encode(block);
}

void my_aes_example() {
    std::vector<uint8_t> key = Botan::hex_decode("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F");
    std::vector<uint8_t> block(1024);
    std::string tmpString = "Hello World! Let's go over 128 bits!";      // I want to encrypt this

    std::unique_ptr<Botan::BlockCipher> cipher(Botan::BlockCipher::create("AES-256"));
    cipher->set_key(key);
    cipher->encrypt((uint8_t*)tmpString.c_str());

    std::cout << std::endl << cipher->name() << ", Single block encrypt: " << Botan::hex_encode((uint8_t *)tmpString.c_str(), tmpString.length());
}

void botan_sandbox() {
    std::vector<uint8_t> key = Botan::hex_decode("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F");
    std::vector<uint8_t> block(1024);
    std::string tmpString = "Hello World";      // I want to encrypt this

    cout << (const char*)block.data() << endl;
    Botan::hex_encode((char*)block.data(), Botan::cast_char_ptr_to_uint8(tmpString.c_str()), tmpString.length());
    //Botan::hex_decode((const char*)block.data());
    cout << (const char*)block.data() << endl;

    cout << Botan::cast_char_ptr_to_uint8("Hello world!") << endl;
//    cout << Botan::cast_uint8_ptr_to_char(&block[0]) << endl;
}

//  ####################################################################################################################
//  ################################                 SANDBOX                 ###########################################
//  ####################################################################################################################

//  ####################################################################################################################
//  ###########################                 LAB IMPLEMENTATION                 #####################################
//  ####################################################################################################################

vector<uint8_t> generateKey(unsigned int keyLen) {
    vector<uint8_t> keyToRet(keyLen);
    Botan::RandomNumberGenerator& botanRng = system_rng();
    botanRng.randomize(keyToRet.data(), keyLen);
    return move(keyToRet);
}

void digitalSignature(const string& message, const vector<uint8_t>& privateKeyToSignWith, HashFunctionChoice hashChoice) {
    // First hash the message
    vector<uint8_t> buf(2048);
    unique_ptr<Botan::HashFunction> hashFunc;

    if(hashChoice == HashFunctionChoice::SHA3_256) {
        hashFunc = Botan::HashFunction::create("SHA-3(256)");
    } else if(hashChoice == HashFunctionChoice::SHA3_512) {
        hashFunc = Botan::HashFunction::create("SHA-3(512)");
    } else {
        throw invalid_argument("Unimplemented HashFunctionChoice!");
    }

    hashFunc->update(message);

    // We got the message, now encrypt it using our private key(RSA algorithm)
    
}

//  ####################################################################################################################
//  ###########################                 LAB IMPLEMENTATION                 #####################################
//  ####################################################################################################################

void cryptoEntry() {
    // Generate Alice and Bob RSA keys and a symmetric key that will be used for encrypting the message
    Botan::RandomNumberGenerator& rng = system_rng();
    RSA_PrivateKey AlicePKey(rng, 2048);

}

int main() {
    cout << "NOS Lab2 - Cryptography!" << endl;

    cryptoEntry();

    return 0;
}