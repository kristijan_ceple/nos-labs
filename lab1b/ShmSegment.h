//
// Created by kikyy_99 on 17.03.21.
//

#ifndef LAB1B_SHMSEGMENT_H
#define LAB1B_SHMSEGMENT_H

#include <cerrno>
#include <sys/mman.h>
#include <cstdio>
#include <string>
#include <memory>
#include <fcntl.h>
#include <fmt/format.h>
#include "Util.h"

namespace ShmSegMng {
    enum class AccessMode{Create, Open, CreateAndOpen};
}

template<class T>
class ShmSegment {
private:
    int fileDescriptor = -1;
    T* mappedPtr = nullptr;
    unsigned int totalSize = 0;
    unsigned int singleSize = 0;
    unsigned int length = 0;
    std::string name;

    void simpleAccessOp(const std::string& name, ShmSegMng::AccessMode accessMode);
public:
    ShmSegment() = delete;
    ShmSegment(int fileDescriptor, T* mappedPtr,
               unsigned int totalSize, unsigned int singleSize, unsigned int length,
               const std::string& name)
            : fileDescriptor{fileDescriptor}, mappedPtr{mappedPtr},
              totalSize{totalSize}, singleSize {singleSize}, length {length},
            name{name} {};
    ShmSegment(const std::string& name, ShmSegMng::AccessMode accessMode, unsigned int N);
    ~ShmSegment();

    T* get();

    friend std::ostream& operator<<(std::ostream& os, ShmSegment<Util::db_entry>& shmSegment);
};

template<class T>
ShmSegment<T>::ShmSegment(const std::string& name, ShmSegMng::AccessMode accessMode, unsigned int N)
        : name{name}, length {N} {
    // Init sizes
    this->singleSize = sizeof(T);
    this->totalSize = this->length * sizeof(T);

    if(accessMode == ShmSegMng::AccessMode::Create || accessMode == ShmSegMng::AccessMode::Open) {
        // Simple operations
        this->simpleAccessOp(name, accessMode);
    } else if(accessMode == ShmSegMng::AccessMode::CreateAndOpen) {
        // Try to open, and if fails then open again -- check or existence
        int res = shm_open(Util::DB_SHM_NAME.c_str(), O_RDWR | O_CREAT | O_EXCL, 0600);
        if(res == -1) {
            if(errno == EEXIST) {
                // Need to open!
                this->simpleAccessOp(name, ShmSegMng::AccessMode::Open);
                return;
            } else {
                perror("Error during Shared Memory Segment creation!");
                throw std::runtime_error(fmt::format("Runtime error! Errno: {}", errno));
            }
        }

        // Unlink then create the shm segment again - make sure to truncate!
        shm_unlink(name.c_str());
        this->simpleAccessOp(name, ShmSegMng::AccessMode::Create);
    } else {
        throw std::invalid_argument(fmt::format("Action for AccessMode: {}, not yet implemented.", (int)accessMode));
    }
}

template<class T>
T *ShmSegment<T>::get() {
    return this->mappedPtr;
}

template<class T>
void ShmSegment<T>::simpleAccessOp(const std::string &name, ShmSegMng::AccessMode accessMode) {
    if(accessMode == ShmSegMng::AccessMode::Create) {
        // Open, and then truncate!
        int res = shm_open(Util::DB_SHM_NAME.c_str(), O_RDWR | O_CREAT | O_EXCL, 0600);
        if(res == -1) {
            perror("Error during Shared Memory Segment creation!");
            throw std::runtime_error(fmt::format("Runtime error! Errno: {}", errno));
        }

        this->fileDescriptor = res;

        // Time to map and truncate!
        ftruncate(this->fileDescriptor, this->totalSize);

        this->mappedPtr = (T*)mmap(
                nullptr, this->totalSize, PROT_READ | PROT_WRITE, MAP_SHARED,
                this->fileDescriptor, 0
        );

        if(this->mappedPtr == MAP_FAILED) {
            perror("Error during pointer mapping!");
            throw std::runtime_error(fmt::format("Runtime error! Errno: {}", errno));
        }
    } else if(accessMode == ShmSegMng::AccessMode::Open) {
        // Just open and save id
        int res = shm_open(Util::DB_SHM_NAME.c_str(), O_RDWR, 0600);
        if(res == -1) {
            perror("Error during Shared Memory Segment creation!");
            throw std::runtime_error(fmt::format("Runtime error! Errno: {}", errno));
        }
        this->fileDescriptor = res;

        // Map the pointer
        this->mappedPtr = (T*)mmap(
                nullptr, this->totalSize, PROT_READ | PROT_WRITE, MAP_SHARED,
                this->fileDescriptor, 0
        );
        if(this->mappedPtr == MAP_FAILED) {
            perror("Error during pointer mapping!");
            throw std::runtime_error(fmt::format("Runtime error! Errno: {}", errno));
        }
    } else {
        throw std::invalid_argument(fmt::format("Action for Simple AccessMode: {}, not yet implemented.",
                                                (int)accessMode));
    }
}

template<class T>
ShmSegment<T>::~ShmSegment() {
    std::cout << fmt::format("Destroying Shared Memory Segment: {}", this->name) << std::endl;

    // Unlink the shared memory segment
    int res = munmap(this->mappedPtr, this->totalSize);
    if(res == -1) {
        std::cerr << fmt::format("Runtime error! Errno: {}", errno) << std::endl;
        perror("Error during pointer un-mapping!");
    }

    res = shm_unlink(this->name.c_str());
    if(res == -1) {
        std::cerr << fmt::format("Runtime error! Errno: {}, Shm Name: {}", errno, this->name) << std::endl;
        perror("Error during ShmSegment shm_unlink!");
    }
}

std::ostream &operator<<(std::ostream &os, ShmSegment<Util::db_entry> &shmSegment) {
    os << "=======================================        !!!!Printing database!!!            =======================================" << std::endl;
    for(int i = 0; i < shmSegment.length; i++) {
        os << shmSegment.get()[i] << std::endl;
    }
    os << "==========================================================================================================================" << std::endl;

    return os;
}

#endif //LAB1B_SHMSEGMENT_H
