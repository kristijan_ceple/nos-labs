#include <iostream>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include "Util.h"
#include "ShmSegment.h"

using namespace std;

void initDb();
void initPipes();
void unlinkPipes(bool programStart = false);
void createProcesses();
void wait4Processes();

vector<pid_t> processIds;
vector<unsigned int> processesStartingLogClocks;
unique_ptr<ShmSegment<Util::db_entry>> dbShmSeg;
size_t N;

int main() {
    N = Util::randomProcesses(Util::engine);
    initDb();
    initPipes();
    createProcesses();
    wait4Processes();
    unlinkPipes();
//    shm_unlink(Util::DB_SHM_NAME.c_str());        // Not necessary, since ShmSeg destructor will take care of it
    return 0;
}

void initDb() {
    // Unlink old db - just in case
    int res = shm_unlink(Util::DB_SHM_NAME.c_str());
    if(res == -1) {
        if(errno == ENOENT) {
            cout << "No leftover Shm Segments found in memory!" << endl;
        } else {
            perror("Error during ShmSegment shm_unlink!");
            throw std::runtime_error(fmt::format("Runtime error! Errno: {}", errno));
        }
    }

    // Create a shared memory segment, put database into it
    dbShmSeg = make_unique<ShmSegment<Util::db_entry>>(
            Util::DB_SHM_NAME.c_str(), ShmSegMng::AccessMode::Create, N);

    unsigned int tmpLogClockVal;
    for(int pid = 0; pid < N; pid++) {
        dbShmSeg->get()[pid].pid = pid;

        tmpLogClockVal = Util::randomLogClock(Util::engine);
        processesStartingLogClocks.emplace_back(tmpLogClockVal);
        dbShmSeg->get()[pid].logClockVal = tmpLogClockVal;
    }

    cout << "Database created in the IPC address space!" << endl;
    cout << *dbShmSeg << endl;
}

void createProcesses() {
    for(int i = 0; i < N; i++) {
        pid_t retVal = fork();
        switch(retVal) {
            case -1:
                // Error!
                cerr << "Unable to create a process! Terminating!" << endl;
                exit(-1);
            case 0:
                execl("./DatabaseProcess", "DatabaseProcess",
                      to_string(i).c_str(), to_string(N).c_str(),
                      to_string(processesStartingLogClocks[i]).c_str(), NULL);
                cerr << "Error during DatabaseProcess execl! Terminating..." << endl;
                exit(-1);
            default:
                // Add to vector
                processIds.emplace_back(retVal);
        }
    }
}

/**
 * Creates named pipes pairwise between each process.
 */
void initPipes() {
    // First close and unlink any old leftover pipes
    unlinkPipes(true);

    // Create new pipes
    int retVal;
    string tmpStr;
    for(int i = 0; i < N; i++) {
        for(int j = i+1; j < N; j++) {
            tmpStr = fmt::format("./{}_pipe_{}_{}", Util::PIPES_PREFIX, i, j);
//            cout << "Creating pipe: " << tmpStr << endl;
            retVal = mkfifo(
                    tmpStr.c_str(),
                    0666
            );
            Util::checkKernelCall(retVal, "Error during mkfifo!");

            tmpStr = fmt::format("./{}_pipe_{}_{}", Util::PIPES_PREFIX, j, i);
//            cout << "Creating pipe: " <<  tmpStr << endl;
            retVal = mkfifo(
                    tmpStr.c_str(),
                    0666
            );
            Util::checkKernelCall(retVal, "Error during mkfifo!");
        }
    }
}

void wait4Processes() {
    for(int i = 0; i < N; i++) {
        wait(NULL);
    }
}

void unlinkPipes(bool programStart) {
    int retVal;
    for(int i = 0; i < N; i++) {
        for(int j = i+1; j < N; j++) {
            if(i == j) {
                continue;
            } else {
                retVal = unlink(fmt::format("./{}_pipe_{}_{}", Util::PIPES_PREFIX, i, j).c_str());
                Util::checkKernelCall(retVal, "Error when unlinking pipe!", programStart);

                retVal = unlink(fmt::format("./{}_pipe_{}_{}", Util::PIPES_PREFIX, j, i).c_str());
                Util::checkKernelCall(retVal, "Error when unlinking pipe!", programStart);
            }
        }
    }
}