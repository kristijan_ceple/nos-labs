#include <iostream>
#include <string>
#include <variant>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include <mqueue.h>
#include <fmt/format.h>
#include "Util.h"

using namespace std;

void createCars(unsigned int N);
void createSemaphore();
void waitForProcesses();

void recreateQueue();

vector<pid_t> carsList;
pid_t semaphoreId;
mqd_t mqId;

int main() {
    recreateQueue();

    // Initiate Autos and Semaphore here
    createCars(Util::randomCars(Util::engine));
//    createCars(3);        // Used for debugging!
    createSemaphore();

    waitForProcesses();

    return 0;
}

void recreateQueue() {
    // Unlink the message queue
    mq_unlink(fmt::format("{}", Util::msgQueueName).c_str());

//    mq_attr mqAttr{
//        .mq_maxmsg = 50,
//        .mq_msgsize = 8192
//    };
    mqId = mq_open(fmt::format("{}", Util::msgQueueName).c_str(), O_RDWR | O_CREAT, 0600,
                        nullptr);
    if(mqId == -1) {
        cerr << "Errno: " << errno << endl;
        perror("Error when opening the message queue!");
        throw runtime_error("Couldn't open the message queue!");
    }

    mq_attr test{};
    mq_getattr(mqId, &test);
}

void createCars(unsigned int N) {
    for(int i = 0; i < N; i++) {
        int carWay = Util::randomWay(Util::engine);
        pid_t retVal = fork();
        switch(retVal) {
            case -1:
                // Error!
                cerr << "Unable to create a process! Terminating!" << endl;
                exit(-1);
            case 0:
            {
                // Generate way and reg
                execl("./Auto", "Auto", to_string(i).c_str(), Util::way2String((Util::Way)carWay), NULL);
                cerr << "Couldn't open program Auto! Terminating..." << endl;
                exit(-1);
            }
            default:
                // Add to vector
                carsList.emplace_back(retVal);
        }
    }
}

void createSemaphore() {
    pid_t retVal = fork();
    switch(retVal) {
        case -1:
            // Error!
            cerr << "Unable to create a process! Terminating!" << endl;
            exit(-1);
        case 0:
            execl("./Semaphore", "Semaphore", NULL);
            cerr << "Couldn't open program Semaphore! Terminating..." << endl;
            exit(-1);
        default:
            // Add to vector
            semaphoreId = retVal;
    }
}

void waitForProcesses() {
    // Wait for them to finish - joining
    for(const auto& carId : carsList) {
        wait(NULL);
    }

    // Signal to Semaphore, and then wait for it too!
    // Make message to send
    Util::JsonData jsonData = {
            .op = (int)Util::Op::Stop,
            .id = 0,
            .way = 0
    };
    string compactMsg = JS::serializeStruct(jsonData, JS::SerializerOptions(JS::SerializerOptions::Compact));

    // Send to msg queue now!
    auto res = mq_send(mqId, compactMsg.c_str(), strlen(compactMsg.c_str())+1, 0);
    if(res == -1) {
        perror("Error during message conveyance!");
        throw runtime_error("Runtime error!");
    }

    // Wait 4 Semaphore process to finish
    waitpid(semaphoreId, nullptr, 0);
}