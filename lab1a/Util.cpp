//
// Created by kikyy_99 on 15.03.21.
//

#include "Util.h"
using namespace std;

void Util::parseJson2MessageStruct(const Util::JsonData &jsonData, ParsedMessage &parseTo) {
    parseTo.id = jsonData.id;
    parseTo.way = static_cast<Util::Way>(jsonData.way);
    parseTo.op = static_cast<Util::Op>(jsonData.op);
}