//
// Created by kikyy_99 on 13.03.21.
//

#ifndef LAB1A_UTIL_H
#define LAB1A_UTIL_H


#include <stdexcept>
#include <cstring>
#include <json_struct.h>
#include <string>
#include <mqueue.h>
#include <random>

class Util {
public:
    enum class Way { Left = 0, Right = 1 };
    enum class Op { Waiting = 0, Go = 1, Passed = 2, Stop = 3 };
    struct JsonData {
        int op;
        unsigned int id;
        int way;

        JS_OBJECT(
                JS_MEMBER(op),
                JS_MEMBER(id),
                JS_MEMBER(way)
                );
    };
    struct ParsedMessage {
        Op op;
        unsigned int id;
        Way way;
    };

    static inline constexpr unsigned int MIN_CARS = 5;
    static inline constexpr unsigned int MAX_CARS = 100;

    static inline constexpr unsigned int MIN_TIMEOUT_MS = 500;
    static inline constexpr unsigned int MAX_TIMEOUT_MS = 1000;

    static inline constexpr unsigned int MAX_CONCURRENT_CROSSING_CARS = 3;
    static inline constexpr unsigned int MIN_CROSS_TIME_MS = 1000;
    static inline constexpr unsigned int MAX_CROSS_TIME_MS = 3000;

    static inline std::random_device rd;
    static inline std::mt19937_64 engine{rd()};
    static inline std::uniform_real_distribution<> randomGenerator{0, 1};
    static inline std::uniform_int_distribution<> randomCars{Util::MIN_CARS, Util::MAX_CARS};
    static inline std::uniform_int_distribution<> randomTimeout{Util::MIN_TIMEOUT_MS, Util::MAX_TIMEOUT_MS};
    static inline std::uniform_int_distribution<> randomCross{Util::MIN_CROSS_TIME_MS, Util::MAX_CROSS_TIME_MS};
    static inline std::uniform_int_distribution<> randomWay{(int)Util::Way::Left, (int)Util::Way::Right};

    static inline const unsigned int TIMEOUT_MS = randomTimeout(engine);

    static inline const std::string msgQueueName = "/NOSlab1a";
//    static inline mq_attr mqAttr {
//        .mq_maxmsg = 1000,
//        .mq_msgsize = 1000000
//    };
//
//    static inline unsigned int MESSAGE_BUFFER_CAPACITY = 1024;
//    static inline std::unique_ptr<char[]> MESSAGE_BUFFER = std::make_unique<char[]>(MESSAGE_BUFFER_CAPACITY);
//    static bool receiveMessage(mqd_t mqId, ParsedMessage&);
//    static bool sendMessage(mqd_t mqId, const ParsedMessage&);

    static const char* way2String(Way toString);
    static const char* op2String(Op toString);
    static Way string2Way(const char* toWay);
    static void parseJson2MessageStruct(const JsonData& jsonData, ParsedMessage &parseTo);
};

inline const char* Util::way2String(Util::Way toString) {
    switch(toString){
        case Way::Left:
            return "Left";
        case Way::Right:
            return "Right";
        default:
            throw std::invalid_argument("String not yet implemented for this Way! Way given: " + std::to_string((int)toString));
    }
}

inline const char* Util::op2String(Util::Op toString) {
    switch(toString){
        case Op::Passed:
            return "Passed";
        case Op::Waiting:
            return "Waiting";
        case Op::Stop:
            return "Stop";
        case Op::Go:
            return "Go";
        default:
            throw std::invalid_argument("String not yet implemented for this Way! Way given: " + std::to_string((int)toString));
    }
}

inline Util::Way Util::string2Way(const char* toWay) {
    if(strcmp(toWay, "Left") == 0) {
        return Way::Left;
    } else if(strcmp(toWay, "Right") == 0) {
        return Way::Right;
    } else {
        throw std::invalid_argument("Cannot convert the given string to Way! Problem when converting: " + std::string(toWay));
    }
}

//JS_OBJ_EXT(
//        Util::JsonData,
//        Op,
//        id,
//        Way,
//);

#endif //LAB1A_UTIL_H
