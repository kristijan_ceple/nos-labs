from enum import Enum
import json
import ast

import Cryptodome
import Cryptodome.Signature
import Cryptodome.Random
from Cryptodome.Signature import pss
from Cryptodome.Hash import SHA3_512, SHA3_256
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import AES, DES3, PKCS1_OAEP

CONFIG_FILE_NAME = "DigitalCertificate1.cfg"
LINES_READ_THRESHOLD = 1000


class AssignmentDescription(Enum):
    DigitalSignature = 0
    DigitalEnvelope = 1
    DigitalCertificate = 2


class SymMethod(Enum):
    AES = 0
    DES3 = 1


class SymMethodMode(Enum):
    CFB = 0
    CTR = 1


class HashFunction(Enum):
    SHA3_256 = 0,
    SHA3_512 = 1


class DataHolder:
    def __init__(self):
        self.description: AssignmentDescription = None

        self.alice_pub_key = None
        self.alice_pvt_key = None
        self.bob_pub_key = None
        self.bob_pvt_key = None

        self.symm_key = None
        self.symm_method: SymMethod = None
        self.symm_method_mode: SymMethodMode = None

        self.symm_key_length = 0
        self.asymm_key_length = 0

        self.hash_function: HashFunction = None

        self.message = ""


def load_data(dh: DataHolder):
    with open(CONFIG_FILE_NAME, 'r') as cfg_file:
        tmp = cfg_file.readline()
        if tmp != "--BEGIN NOS CRYPTO DATA--\n":
            raise RuntimeError("File header format wrong?")

        class CheckFormat:
            def __init__(self):
                self.Description_valid = False
                self.SymMethod_valid = False
                self.SymMethodMode_valid = False
                self.SymKeyLength_valid = False
                self.AsymKeyLength_valid = False
                self.HashFunction_valid = False
                self.Message_valid = False
        check_format = CheckFormat()

        lines_read = 0
        while True:
            line = cfg_file.readline().rstrip()
            lines_read += 1

            # Check if end
            if line == '--END NOS CRYPTO DATA--':
                break
            elif lines_read == LINES_READ_THRESHOLD:
                raise RuntimeError("Lines read threshold reached!")

            key, value = line.split(":")
            if key == 'Description':
                if check_format.Description_valid is True:
                    raise RuntimeError("File incorrect format! Duplicate detected!")
                elif value == 'DigitalSignature':
                    dh.description = AssignmentDescription.DigitalSignature
                elif value == 'DigitalEnvelope':
                    dh.description = AssignmentDescription.DigitalEnvelope
                elif value == 'DigitalCertificate':
                    dh.description = AssignmentDescription.DigitalCertificate
                else:
                    raise RuntimeError("File incorrect format!")
                check_format.Description_valid = True
            elif key == 'SymMethod':
                if check_format.SymMethod_valid is True:
                    raise RuntimeError("File incorrect format! Duplicate detected!")
                elif value == 'AES':
                    dh.symm_method = SymMethod.AES
                elif value == '3DES':
                    dh.symm_method = SymMethod.DES3
                else:
                    raise RuntimeError("File incorrect format!")
                check_format.SymMethod_valid = True
            elif key == 'SymMethodMode':
                if check_format.SymMethodMode_valid is True:
                    raise RuntimeError("File incorrect format! Duplicate detected!")
                elif value == 'CFB':
                    dh.symm_method_mode = SymMethodMode.CFB
                elif value == 'CTR':
                    dh.symm_method_mode = SymMethodMode.CTR
                else:
                    raise RuntimeError("File incorrect format!")
                check_format.SymMethodMode_valid = True
            elif key == 'SymKeyLength':
                # Possible key lengths depend whether AES or 3DES was selected
                # Possible AES key lengths: 128, 192 or 256
                # Possible 3DES key lengths:
                if check_format.SymKeyLength_valid is True:
                    raise RuntimeError("File incorrect format! Duplicate detected!")
                elif dh.symm_method == SymMethod.AES:
                    value = int(value)

                    if value in (128, 192, 256):
                        dh.symm_key_length = value
                    else:
                        raise RuntimeError("Unallowed AES Key length!")
                elif dh.symm_method == SymMethod.DES3:
                    value = int(value)

                    if value in (112, 168):
                        dh.symm_key_length = value
                    else:
                        raise RuntimeError("Unallowed 3DES Key length!")
                check_format.SymKeyLength_valid = True
            elif key == 'AsymKeyLength':
                if check_format.AsymKeyLength_valid is True:
                    raise RuntimeError("File incorrect format! Duplicate detected!")
                else:
                    dh.asymm_key_length = int(value)
                check_format.AsymKeyLength_valid = True
            elif key == 'HashFunction':
                if check_format.HashFunction_valid is True:
                    raise RuntimeError("File incorrect format! Duplicate detected!")
                elif value == "SHA3-256":
                    dh.hash_function = HashFunction.SHA3_256
                elif value == "SHA3-512":
                    dh.hash_function = HashFunction.SHA3_512
                else:
                    raise RuntimeError("File incorrect format!")
                check_format.HashFunction_valid = True
            elif key == "Message" and value == "{":
                while True:
                    # Need to read Message until the terminating "}"
                    line = cfg_file.readline()
                    lines_read += 1

                    # Check if end
                    if line.rstrip() == '}':
                        break
                    elif lines_read == LINES_READ_THRESHOLD:
                        raise RuntimeError("Lines read threshold reached!")

                    dh.message += line
                check_format.Message_valid = True

        # Done, now check the format
        #check_format_fields = [a for a in dir(check_format) if (not a.startswith("__") and not callable(getattr(check_format, a)))]
        if False in (check_format.Description_valid, check_format.AsymKeyLength_valid, check_format.SymMethod_valid,
                     check_format.SymKeyLength_valid, check_format.HashFunction_valid,
                     check_format.SymMethodMode_valid, check_format.Message_valid):
            raise RuntimeError("File incorrect format!")

        # Everything good! Proceed
        return


########################################################################################################################
########################################            DIGITAL SIGNATURE           ########################################
########################################################################################################################
def alice_digital_sign(dh: DataHolder, msg: str):
    # digital_signature = {m, Encrypt(Hash(m))}
    hasher = None
    if dh.hash_function == HashFunction.SHA3_256:
        hasher = SHA3_256.new()
    elif dh.hash_function == HashFunction.SHA3_512:
        hasher = SHA3_512.new()
    else:
        raise RuntimeError("execute_cfg not implemented for this HashFunction!")

    hasher.update(msg.encode('utf-8'))

    # Now sign, and write the message into a file
    # signature = Cryptodome.Signature.PKCS1_v1_5.new(dh.alice_pvt_key)
    signature = pss.new(dh.alice_pvt_key)
    signed_digest = signature.sign(hasher)

    result = {
        'msg': msg.encode('utf-8'),
        'signed_digest': signed_digest
    }
    return result


def bob_digital_verify(alice_pub_key, result, hash_function: HashFunction):
    message = result['msg']
    signed_digest = result['signed_digest']

    hasher = None
    if hash_function == HashFunction.SHA3_256:
        hasher = SHA3_256.new()
    elif hash_function == HashFunction.SHA3_512:
        hasher = SHA3_512.new()
    else:
        raise RuntimeError("execute_cfg not implemented for this HashFunction!")
    hasher.update(message)

    try:
        verifier = pss.new(alice_pub_key)
        verifier.verify(hasher, signed_digest)
        print("Bob successfully verified Alice's Signature!")
    except(ValueError, TypeError) as e:
        print(e)
        print("!!!ERROR!!!: Bob DIDN'T successfully verify Alice's Signature!")
        raise e


def digital_signature(dh: DataHolder, msg: str):
    print("Alice will sign, Bob will verify!\n")
    result = alice_digital_sign(dh, msg)
    print("Alice's Digital Signature: " + str(result))

    print("\nTime for Bob to verify!")
    bob_digital_verify(dh.alice_pub_key, result, dh.hash_function)

########################################################################################################################
########################################            DIGITAL SIGNATURE           ########################################
########################################################################################################################


########################################################################################################################
########################################            DIGITAL ENVELOPE           #########################################
########################################################################################################################
def alice_digital_envelope(dh: DataHolder):
    asym_cipher = PKCS1_OAEP.new(dh.bob_pub_key)
    sym_cipher = None
    iv_nonce = None         # CFB has iv, CTR has nonce
    if dh.symm_method == SymMethod.AES:
        if dh.symm_method_mode == SymMethodMode.CFB:
            sym_cipher = AES.new(dh.symm_key, AES.MODE_CFB)
            iv_nonce = sym_cipher.iv
        elif dh.symm_method_mode == SymMethodMode.CTR:
            sym_cipher = AES.new(dh.symm_key, AES.MODE_CTR)
            iv_nonce = sym_cipher.nonce
        else:
            raise RuntimeError("execute_cfg not implemented for this SymMethodMode!")
    elif dh.symm_method == SymMethod.DES3:
        if dh.symm_method_mode == SymMethodMode.CFB:
            sym_cipher = DES3.new(dh.symm_key, DES3.MODE_CFB)
            iv_nonce = sym_cipher.iv
        elif dh.symm_method_mode == SymMethodMode.CTR:
            sym_cipher = DES3.new(dh.symm_key, DES3.MODE_CTR, nonce=Cryptodome.Random.get_random_bytes(1))
            iv_nonce = sym_cipher.nonce
        else:
            raise RuntimeError("execute_cfg not implemented for this SymMethodMode!")
    else:
        raise RuntimeError("execute_cfg not implemented for this SymMethod!")

    # Now we need to encrypt the message using the sym_cipher
    enc_msg = sym_cipher.encrypt(dh.message.encode('utf-8'))
    # Now encrypt Sym_Key using Bob's public key
    encrypted_sym_key = asym_cipher.encrypt(dh.symm_key)

    result = {'iv_nonce': iv_nonce, 'enc_msg': enc_msg, 'enc_sym_key': encrypted_sym_key}
    return result


def bob_digital_envelope(bob_pvt_key, result, symm_method, symm_method_mode):
    iv_nonce = result['iv_nonce']
    enc_msg = result['enc_msg']
    enc_sym_key = result['enc_sym_key']

    # Create (De)Cipher objects
    cipher_rsa = PKCS1_OAEP.new(bob_pvt_key)
    sym_cipher = None

    # Get the sym_key using the RSA cipher
    sym_key = cipher_rsa.decrypt(enc_sym_key)

    if symm_method == SymMethod.AES:
        if symm_method_mode == SymMethodMode.CFB:
            sym_cipher = AES.new(sym_key, AES.MODE_CFB, iv=iv_nonce)
        elif symm_method_mode == SymMethodMode.CTR:
            sym_cipher = AES.new(sym_key, AES.MODE_CTR, nonce=iv_nonce)
        else:
            raise RuntimeError("execute_cfg not implemented for this SymMethodMode!")
    elif symm_method == SymMethod.DES3:
        if symm_method_mode == SymMethodMode.CFB:
            sym_cipher = DES3.new(sym_key, DES3.MODE_CFB, iv=iv_nonce)
        elif symm_method_mode == SymMethodMode.CTR:
            sym_cipher = DES3.new(sym_key, DES3.MODE_CTR, nonce=iv_nonce)
        else:
            raise RuntimeError("execute_cfg not implemented for this SymMethodMode!")
    else:
        raise Exception("Invalid SymMethod")

    # Now decrypt the message and return it
    msg = sym_cipher.decrypt(enc_msg)
    return msg.decode('utf-8')


def digital_envelope(dh: DataHolder):
    # digital_envelope = {E(m, Sym_Key), E(Sym_Key, Bob_Pub_Key)}
    print("Alice will form a digital envelope with the message, Bob will read the message!\n")
    print("Alice sends message: " + dh.message)
    result = alice_digital_envelope(dh)

    print("\nAlice sends to Bob: " + str(result) + "\n")

    bob_decrypted_msg = bob_digital_envelope(dh.bob_pvt_key, result, dh.symm_method, dh.symm_method_mode)
    print("\nBob reads message: " + bob_decrypted_msg)
    return result
########################################################################################################################
########################################            DIGITAL ENVELOPE           #########################################
########################################################################################################################


########################################################################################################################
#####################################            DIGITAL CERTIFICATE           #########################################
########################################################################################################################
def digital_certificate_old(dh: DataHolder):
    # Digital signature, where m is digital envelope
    m = digital_envelope(dh)
    digital_signature(dh, str(m))


def digital_certificate(dh: DataHolder):
    # Digital signature, where m is digital envelope
    # Alice part
    print("Alice is making a digital envelope - and signing it!")
    print("Alice sends to Bob a message: " + dh.message)
    m = alice_digital_envelope(dh)
    m = alice_digital_sign(dh, str(m))

    print("\nAlice sends to Bob a digital certificate: " + str(m) + '\n')

    # Bob part
    # He must first parse bytes into python dict
    parsed_dict = m['msg'].decode('utf-8')
    parsed_dict = ast.literal_eval(parsed_dict)
    bob_digital_verify(dh.alice_pub_key, m, dh.hash_function)
    bob_decrypted_msg = bob_digital_envelope(dh.bob_pvt_key, parsed_dict, dh.symm_method, dh.symm_method_mode)
    print("\nBob reads message: " + bob_decrypted_msg)


########################################################################################################################
#####################################            DIGITAL CERTIFICATE           #########################################
########################################################################################################################


def prepare_environment(dh: DataHolder):
    # Generate Alice and Bob keys, and a symmetric key - fill the DataHolder basically

    # If 3DES we have to adjust parity bits
    if dh.symm_method == SymMethod.DES3:
        # 3DES
        while True:
            tmp_key = None
            if dh.symm_key_length == 112:
                tmp_key = Cryptodome.Random.get_random_bytes(16)
            elif dh.symm_key_length == 168:
                tmp_key = Cryptodome.Random.get_random_bytes(24)
            else:
                raise Exception("Unallowed 3DES key length!?")

            try:
                dh.symm_key = DES3.adjust_key_parity(tmp_key)
                break
            except ValueError:
                continue
    else:
        # AES
        tmp_key = Cryptodome.Random.get_random_bytes(int(dh.symm_key_length / 8))
        dh.symm_key = tmp_key

    # Now Alice keys
    dh.alice_pvt_key = RSA.generate(dh.asymm_key_length)
    dh.bob_pvt_key = RSA.generate(dh.asymm_key_length)

    # Now Bob keys
    dh.alice_pub_key = dh.alice_pvt_key.public_key()
    dh.bob_pub_key = dh.bob_pvt_key.public_key()


def execute_cfg(dh: DataHolder):
    if dh.description == AssignmentDescription.DigitalSignature:
        digital_signature(dh, dh.message)
    elif dh.description == AssignmentDescription.DigitalEnvelope:
        digital_envelope(dh)
    elif dh.description == AssignmentDescription.DigitalCertificate:
        digital_certificate(dh)
    else:
        raise RuntimeError("execute_cfg not implemented for this SymMethod!")


def main():
    dh = DataHolder()
    load_data(dh)
    prepare_environment(dh)
    execute_cfg(dh)


if __name__ == "__main__":
    main()
