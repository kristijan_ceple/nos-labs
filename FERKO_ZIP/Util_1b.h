//
// Created by kikyy_99 on 17.03.21.
//

#ifndef LAB1B_UTIL_H
#define LAB1B_UTIL_H


#include <random>
#include <memory>

class Util {
public:
    enum class MessageType {Request, Answer, CSExit, CSnProgramExit};

    // Random - related aspects
    static inline constexpr unsigned int MIN_PROCESSES = 3;
    static inline constexpr unsigned int MAX_PROCESSES = 10;
    static inline constexpr unsigned int PROCESS_CS_ENTRIES = 5;

    static inline constexpr unsigned int PROCESS_TASKS_NUM = 10;
    static inline constexpr unsigned int MESSAGE_PARTS_NUM = 10;

    static inline constexpr unsigned int MIN_SLEEP_MS = 100;
    static inline constexpr unsigned int MAX_SLEEP_MS = 2000;

    static inline constexpr unsigned int MIN_LOG_CLOCK = 0;
    static inline constexpr unsigned int MAX_LOG_CLOCK = 77777;          // 77777

    static inline std::random_device rd;
    static inline std::mt19937_64 engine{rd()};
    static inline std::uniform_real_distribution<> randomGenerator{0, 1};
    static inline std::uniform_int_distribution<> randomProcesses{Util::MIN_PROCESSES, Util::MAX_PROCESSES};
    static inline std::uniform_int_distribution<> randomSleep{Util::MIN_SLEEP_MS, Util::MAX_SLEEP_MS};
    static inline std::uniform_int_distribution<> randomLogClock{Util::MIN_LOG_CLOCK, Util::MAX_LOG_CLOCK};

    // Database Entry for the Lamport protocol
    struct db_entry {
        pid_t pid;
        unsigned int logClockVal;
        unsigned int entriesNum;

        friend std::ostream& operator<<(std::ostream& os, const db_entry& dbe);
    };
    static inline const std::string DB_SHM_NAME = "/lab1bDatabase";
    static inline const std::string PIPES_PREFIX = "lab1b";

    static void checkKernelCall(int val, const std::string& errorMsg, bool programStart = false);

    static std::string MessageType2String(MessageType toConv);
    static MessageType String2MessageType(const std::string& toConv);

//    static std::string printPrioQ(std::priority_queue<DatabaseProcess::Request> sortedRequests)
};


#endif //LAB1B_UTIL_H
