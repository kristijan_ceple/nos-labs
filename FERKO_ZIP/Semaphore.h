//
// Created by kikyy_99 on 16.03.21.
//

#ifndef LAB1A_SEMAPHORE_H
#define LAB1A_SEMAPHORE_H


#include <queue>
#include "Util.h"

class Semaphore {
private:
    Util::JsonData jsonData{};
    Util::ParsedMessage parsedMessage{};

    Util::Way currentWay = Util::Way::Left;
    std::vector<unsigned int> carsQ;

    mqd_t mqId;
    timespec abs_timeout{
        .tv_sec = 0,
        .tv_nsec = Util::TIMEOUT_MS*1000
    };

    std::unique_ptr<char[]> messageBuffer;
    mq_attr mqAttr;
    size_t messageBufferSize;
//    char* messageBuffer = new char[Util::MESSAGE_BUFFER_CAPACITY];
public:
    explicit Semaphore(mqd_t mqId);
    void run();
};


#endif //LAB1A_SEMAPHORE_H
