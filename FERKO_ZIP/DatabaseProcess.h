//
// Created by kikyy_99 on 17.03.21.
//

#ifndef LAB1B_DATABASEPROCESS_H
#define LAB1B_DATABASEPROCESS_H


#include <queue>
#include "ShmSegment.h"

class DatabaseProcess {
public:
    struct Request {
        unsigned int PID;
        unsigned int logClock;

        Request(unsigned int PID, unsigned int log_clock) : PID{PID}, logClock{log_clock} {};
        std::string to_string();
        std::string pretty_string();
    };

    struct RequestCompare {
        constexpr bool operator()(const Request &lhs, const Request &rhs) const
        {
            if(lhs.logClock != rhs.logClock) {
                return lhs.logClock > rhs.logClock;
            } else {
                return lhs.PID > rhs.PID;
            }
        }
    };
private:
    unsigned int id;
    unsigned int logClock;
    unsigned int requestAnswers;
    unsigned int CSEntries = 0;

    std::unique_ptr<ShmSegment<Util::db_entry>> dbShmSeg;

    size_t sysProcessesNum;
    std::vector<int> inputPipes;
    std::vector<int> outputPipes;

    std::vector<bool> activeProcesses;
    size_t sysActiveProcessesNum;

    std::priority_queue<Request, std::vector<Request>, RequestCompare> sortedRequests;
    void openPipes();
    void printPipes();

    int receiveMessage(int pid);
    int readMessage(int pid);
    int writeMessage(unsigned int pid);
    void broadcastMessage();
    int writeMessage(int pid, const std::string& toWrite);
    bool parseMessage();
    void processMessage();
    inline void logClockUpdate(unsigned int log_clock);

    std::unique_ptr<char[]> charBuffer;
    static inline constexpr size_t BUFFER_SIZE = 1024;
    std::string stringBuffer;

    /**
     * 1. MessageType(enum - int), 2. PID(int), 3. Logical clock(int)
     * Acronym: MPL
     * CSV Format, msg example: "0,57,3456,\0xxxxxxxxxxxxxxxxxxx"
     */
     struct ParsedMessage {
         Util::MessageType messageType;
         unsigned int PID;
         unsigned int logClock;

         std::string to_string();
     };
    ParsedMessage parsedMessage;
     int cached_errno;
public:
    DatabaseProcess(unsigned int id, size_t sysProcessesNum, unsigned int startingLogClock);
    void run();
    void debug_run();
    void lamportProtocolIter(int iterNum);

    void enterCS();

    std::string prioQStr(std::priority_queue<Request, std::vector<Request>, RequestCompare> prioQ);
};

void DatabaseProcess::logClockUpdate(unsigned int other_log_clock) {
    std::cout
    << fmt::format(
            "Database Process ID: {} updating Logical Clock --> max({}, {}) + 1",
            this->id, this->logClock, other_log_clock
            )
    << std::endl;
    this->logClock = std::max(this->logClock, other_log_clock) + 1;
}

#endif //LAB1B_DATABASEPROCESS_H
