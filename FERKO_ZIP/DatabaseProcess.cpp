//
// Created by kikyy_99 on 17.03.21.
//

#include <stdexcept>
#include <memory>
#include <iostream>
#include <sstream>
#include <thread>
#include <fcntl.h>
#include <unistd.h>
#include <fmt/format.h>
#include "DatabaseProcess.h"
#include "Util.h"
using namespace std;

unique_ptr<DatabaseProcess> me;

int main(int argc, char* argv[]) {
    // Takes ID as parameter
    if(argc != 4) {
        throw length_error("Invalid amount of arguments!");
    }

    unsigned int id = stoul(argv[1]);
    size_t sysProcessesNum = stoul(argv[2]);
    unsigned int startingLogClock = stoul(argv[3]);

    me = make_unique<DatabaseProcess>(id, sysProcessesNum, startingLogClock);
    me->run();
}

//      ################################################################################################################
//      ###################################     CLASS AUTO DEFINITIONS      ############################################
//      ################################################################################################################

DatabaseProcess::DatabaseProcess(unsigned int id, size_t sysProcessesNum, unsigned int startingLogClock)
: id{id}, sysProcessesNum{sysProcessesNum}, logClock{startingLogClock} {
    this->charBuffer = make_unique<char[]>(DatabaseProcess::BUFFER_SIZE);

    // Each process has to access and map the shared memory segment!
    this->dbShmSeg = make_unique<ShmSegment<Util::db_entry>>(
            Util::DB_SHM_NAME.c_str(), ShmSegMng::AccessMode::CreateAndOpen, this->sysProcessesNum
            );

    // Prepare active processes vector
    for(int i = 0; i < this->sysProcessesNum; i++) {
        this->activeProcesses.emplace_back(true);
    }
    this->sysActiveProcessesNum = this->sysProcessesNum;
}

void DatabaseProcess::run() {
    cout << fmt::format("Database Process ID: {} started, starting Logical Clock value: {}", this->id, this->logClock) << endl;
    me->openPipes();
//    me->printPipes();

    // Pipes opened - proceed to Lamport protocol
    for(int i = 0; i < Util::PROCESS_CS_ENTRIES; i++) {
        this->lamportProtocolIter(i);
        this_thread::sleep_for(chrono::milliseconds{Util::randomSleep(Util::engine)});
    }

    cout << fmt::format("Database Process ID: {} finished!", this->id) << endl;
}

void DatabaseProcess::openPipes() {
    int retVal = -1;
    string tmpStr;
    for(size_t i = 0; i < this->sysProcessesNum; i++) {
        if(i != this->id) {
            /*
             * Input pipes - read from!
             */
            //            cout << fmt::format("Process ID {} opening pipe for reading", this->id) << endl;
            tmpStr = fmt::format("./{}_pipe_{}_{}", Util::PIPES_PREFIX, i, this->id);
//            cerr << "Opening pipe: " << tmpStr << endl;
            retVal = open(
                    tmpStr.c_str(),
                    O_RDONLY | O_NDELAY | O_CREAT | O_TRUNC,
                    0666
            );
//            cerr << retVal << endl;
            Util::checkKernelCall(retVal, "Error while opening output pipe!");
            this->inputPipes.emplace_back(retVal);

            /*
             * Output pipes - write into!
             */
            tmpStr = fmt::format("./{}_pipe_{}_{}", Util::PIPES_PREFIX, this->id, i);
            do {
//              cout << fmt::format("Process ID {} opening pipe for writing", this->id) << endl;
//              cerr << "Opening pipe: " << tmpStr << endl;
                retVal = open(
                        tmpStr.c_str(),
                        O_WRONLY | O_NDELAY | O_CREAT | O_TRUNC,
                        0666
                );

                if(retVal == -1 && errno != ENXIO) {
                    cerr << "Errno: " << errno << endl;
                    perror("Error while opening pipe for writing!");
                    exit(-1);
                }

            } while(retVal == -1);
//            cerr << retVal << endl;
            this->outputPipes.emplace_back(retVal);
        } else {
            this->inputPipes.emplace_back(-1);
            this->outputPipes.emplace_back(-1);
        }
    }
}

/**
 * Used for debugging!
 */
void DatabaseProcess::printPipes() {
    cout << fmt::format("Database Process ID: {} printing pipes!", this->id) << endl;

    for(int i = 0; i < this->sysProcessesNum; i++) {
        cout << this->inputPipes[i] << ' ' << this->outputPipes[i] << endl;
    }

    cout << endl;
}

/**
 * Reads the message into the buffer
 *
 * @param pid The PID of the process of which the message will be read
 * @return Number of bytes read, or -1 if failure
 */
inline int DatabaseProcess::readMessage(int pid) {
    int retVal = this->inputPipes[pid];
    retVal = read(retVal, this->charBuffer.get(), DatabaseProcess::BUFFER_SIZE);
    this->cached_errno = errno;
    Util::checkKernelCall(retVal, "Error while reading message from the buffer!");
    return retVal;
}

/**
 * Requirement: message located in this->charBuffer(ready to be written)
 *
 * Reads the message from the this->charBuffer and parses it into the triple-element tuple this->parsedMessage
 * MPL - Message,PID, Logical clock
 *
 * @return true if success, false otherwise
 */
bool DatabaseProcess::parseMessage() {
    // Message is located in the buffer, need to split it into the tuple
    stringstream ss{this->charBuffer.get()};
    string tmp;

    if(!getline(ss, tmp, ',')) {
//        throw logic_error("Getline fail during message parsing!?");
        return false;
    }
    this->parsedMessage.messageType = static_cast<Util::MessageType>(strtol(tmp.c_str(), nullptr, 10));

    if(!getline(ss, tmp, ',')) {
//        throw logic_error("Getline fail during message parsing!?");
        return false;
    }
    this->parsedMessage.PID = strtol(tmp.c_str(), nullptr, 10);

    if(!getline(ss, tmp, ',')) {
//        throw logic_error("Getline fail during message parsing!?");
        return false;
    }
    this->parsedMessage.logClock = strtol(tmp.c_str(), nullptr, 10);
    return true;
}

/**
 * Requirement: None
 *
 * @param pid The PID of the process that will receive the message
 * @param toWrite The string which will be sent to the process
 * @return Number of bytes written, -1 if failure
 */
int DatabaseProcess::writeMessage(int pid, const std::string& toWrite) {
    int retVal = this->outputPipes[pid];
    strcpy(this->charBuffer.get(), toWrite.c_str());
    retVal = write(retVal, toWrite.c_str(), DatabaseProcess::BUFFER_SIZE);
    this->cached_errno = errno;
    Util::checkKernelCall(retVal, "Error while reading message from the buffer!");
    return retVal;
}

/**
 * The distributed lamport protocol. Follows the algorithm:
 *      1. Send a (R)Request to enter the Critical Section(CS)
 *      2. Wait to get all the (A)Answers
 *      3. Enter the CS, update my entry
 *      4. Send the (E)Exit message to notify others that they can enter the CS
 *      5. Return from this iteration
 *
 *      Do it all by myself, or have a separate thread taking messages??? Really not sure.
 *      For starters, let me do it all by myself fuck yeah just have to get this over with
 *
 *      Keep a priority queue of (sorted) requests!
 */
void DatabaseProcess::lamportProtocolIter(int iterNum) {
    /*
     * Step 1 --> Send a (R)Request to enter the Critical Section(CS)
     */
    cout << fmt::format("Database Process ID: {} reached Step 1 --> Sending a Request to other processes!", this->id) << endl;

    // Send a request to other processes first, add to my own priority queue
    Request newReq = {
      this->id,
      this->logClock
    };

    // Now send the message to all other processes
    strcpy(this->charBuffer.get(), newReq.to_string().c_str());
    this->sortedRequests.emplace(newReq);
    this->broadcastMessage();

    /*
     * Step 2 --> Wait to get all the (A)Answers
     */
    cout << fmt::format("Database Process ID: {} reached Step 2 --> Waiting for Answers!", this->id) << endl;
    // Now wait till we get all the answers
    this->requestAnswers = 0;
    while(this->requestAnswers < this->sysActiveProcessesNum - 1) {
//        cout << fmt::format("Database Process ID: {} in reqAns while!", this->id) << endl;

        // Receive messages in a circular fashion
        for(int pid = 0; pid < this->sysProcessesNum; pid++) {
            if(pid != this->id && this->activeProcesses[pid]) {
                this->receiveMessage(pid);
            }
        }

//        cout << fmt::format("Inside while: Database Process ID: {} -> reqAns: {}, sysActProcs: {}.",
//                            this->id, this->requestAnswers, this->sysActiveProcessesNum) << endl;
    }

    /*
     * Step 3 --> Enter the CS, update my entry
     */
    cout << fmt::format("Database Process ID: {} reached Step 3 --> Before entering CS!", this->id) << endl;
    // We got all the answers - wait until our request is at the top of the q
    while(true) {
//        cout << fmt::format("Database Process ID: {} in enterCS while!", this->id) << endl;

        // Check if our req is at the top
        const Request& tmp = this->sortedRequests.top();

        if(tmp.PID == this->id) {
            // Can Enter Critical Section
            this->enterCS();
            // Remove my request from the top
            cout << fmt::format("Database Process ID: {} CS-Exit queue popping!", this->id, this->parsedMessage.PID) << endl;
            cout << this->prioQStr(this->sortedRequests) << endl;
            this->sortedRequests.pop();
            break;
        }

        // Read messages, and respond to them. (What we really want to happen here is to receive Exited messages!)
        for(int pid = 0; pid < this->sysProcessesNum; pid++) {
            if(pid != this->id && this->activeProcesses[pid]) {
                this->receiveMessage(pid);
            }
        }
    }

    /*
     * Step 4 --> Send the (E)Exit message to notify others that they can enter the CS
     */
    cout << fmt::format("Database Process ID: {} reached Step 4 --> Sending Exit message to notify others!", this->id) << endl;

    if(iterNum != Util::PROCESS_CS_ENTRIES - 1) {
        this->parsedMessage = {
                .messageType = Util::MessageType::CSExit,
                .PID = this->id,
                .logClock = this->logClock
        };
    } else {
        this->parsedMessage = {
                .messageType = Util::MessageType::CSnProgramExit,
                .PID = this->id,
                .logClock = this->logClock
        };
    }

    strcpy(this->charBuffer.get(), this->parsedMessage.to_string().c_str());
    this->broadcastMessage();

    /**
     * Step 5 --> Return from iteration
     */
     return;
}

/**
 * Requirement: message located in this->charBuffer(ready to be written)
 *
 * Sends the message to all processes!
 */
void DatabaseProcess::broadcastMessage() {
    for(int pid = 0; pid < this->sysProcessesNum; pid++) {
        if(pid != this->id && this->activeProcesses[pid]) {
            this->writeMessage(pid);
        } else {
            continue;
        }
    }
}

/**
 * Requirement: message located in this->charBuffer(ready to be written)
 *
 * The message is located in the charBuffer, just send it to the corresponding process(specified by PID)
 * @return number of bytes written
 */
int DatabaseProcess::writeMessage(unsigned int pid) {
    int retVal = this->outputPipes[pid];
    retVal = write(retVal, this->charBuffer.get(), DatabaseProcess::BUFFER_SIZE);
    this->cached_errno = errno;
    Util::checkKernelCall(retVal, "Error while reading message from the buffer!");
    return retVal;
}

/**
 * Requirement: Message has been parsed
 *
 * If the message has already been parsed and is located in the this->parsedMessage buffer,
 * then this function will process the buffered data and perform certain actions based on the input buffered data
 */
void DatabaseProcess::processMessage() {
    this->logClockUpdate(this->parsedMessage.logClock);

    // Remember, MPL - message, process, logClock
    switch(this->parsedMessage.messageType) {
        case Util::MessageType::Request:
        {
            cout << fmt::format("Database Process ID: {} received Request from Database Process ID: {}", this->id, this->parsedMessage.PID) << endl;

            // Add to priority queue
            this->sortedRequests.emplace(
                    this->parsedMessage.PID,
                    this->parsedMessage.logClock
            );

            // Send answer to the process which send the Request!
            unsigned int tmpPID = this->parsedMessage.PID;
            this->parsedMessage = {
                    .messageType = Util::MessageType::Answer,
                    .PID = this->id,
                    .logClock = this->logClock
            };
            strcpy(this->charBuffer.get(), this->parsedMessage.to_string().c_str());
            this->writeMessage(tmpPID);
            break;
        }
        case Util::MessageType::Answer:
            cout << fmt::format("Database Process ID: {} received Answer message from Database Process ID: {}!", this->id, this->parsedMessage.PID) << endl;
            this->requestAnswers++;
            break;
        case Util::MessageType::CSnProgramExit:
            cout << fmt::format("Database Process ID: {} received CS and Program Exit message from Database Process ID: {}!", this->id, this->parsedMessage.PID) << endl;
            this->sysActiveProcessesNum--;
            this->activeProcesses[this->parsedMessage.PID] = false;

            cout << fmt::format("CSnProgramExit Database Process ID: {} -> reqAns: {}, sysActProcs: {}.",
                                this->id, this->requestAnswers, this->sysActiveProcessesNum) << endl;
        case Util::MessageType::CSExit:
            cout << fmt::format("Database Process ID: {} received CS Exit message from Database Process ID: {}!", this->id, this->parsedMessage.PID) << endl;
            if(this->parsedMessage.PID == this->id) {
                throw logic_error("Process received its own CSExit via some other process!?");
            } else{
                // Remove from priority queue
                cout << fmt::format("Database Process ID: {} CS-Exit queue popping!", this->id, this->parsedMessage.PID) << endl;
                cout << this->prioQStr(this->sortedRequests) << endl;
                this->sortedRequests.pop();
                break;
            }
        default:
            throw invalid_argument("Message parsing not implemented for this MessageType!");
    }
}

void DatabaseProcess::enterCS() {
    // At location PID in IPC Database --> Increment accesses, update my current logical clock value
    cout << endl << fmt::format("Process ID: {} entering Critical Section!", this->id) << endl;

    // Update entries
    this->dbShmSeg->get()[this->id].entriesNum++;
    this->dbShmSeg->get()[this->id].logClockVal = this->logClock;

    // Print database
    cout << endl;
    cout << *dbShmSeg << endl;
    cout << endl;

    // Check if any process has finished! -- Leftover!
//    for(int i = 0; i < this->sysProcessesNum; i++) {
//        if(i != this->id && this->dbShmSeg->get()[this->id].entriesNum == Util::PROCESS_CS_ENTRIES) {
//            this->sysActiveProcessesNum--;
//            this->activeProcesses[i] = false;
//        }
//    }

    cout << fmt::format("Process ID: {} leaving Critical Section!", this->id) << endl << endl;
}

/**
 * MPL format - Message, PID, Log clock
 * @return formatted string
 */
std::string DatabaseProcess::Request::to_string() {
    return fmt::format("{},{},{},", (int)Util::MessageType::Request, this->PID, this->logClock);
}

std::string DatabaseProcess::Request::pretty_string() {
    return fmt::format("Request => MessageType: {}, PID: {}, Logical Clock: {}", "Request", this->PID, this->logClock);
}

/**
 * MPL format - Message, PID, Log clock
 * @return formatted string
 */
std::string DatabaseProcess::ParsedMessage::to_string() {
    return fmt::format("{},{},{},", (int)this->messageType, this->PID, this->logClock);
}

// #####################################################################################################################
// #####################################################################################################################
// #####################################################################################################################

/**
 * Method that does the complete Message Action procedure - reads the message, parses it, and acts upon it
 *
 * @param pid The process from which message will be received
 * @return Number of bytes read
 */
inline int DatabaseProcess::receiveMessage(int pid) {
    int retval = this->readMessage(pid);         // First read into buffer
    if(retval > 0) {
        // If any bytes were read
        this->parseMessage();           // Then parse into our tuple
        this->processMessage();         // Process it
    }

//    cout << fmt::format("Database Process ID: {} -> reqAns: {}, sysActProcs: {}.",
//                        this->id, this->requestAnswers, this->sysActiveProcessesNum) << endl;

    return retval;
}

void DatabaseProcess::debug_run() {
    cout << "Pipes opened!" << endl;

    // For starters - send messages to all processes
    for(int out : this->outputPipes) {
        this->stringBuffer = fmt::format("Hello from Process ID: {}", this->id);
        write(out, this->stringBuffer.c_str(), strlen(this->stringBuffer.c_str())+1);
    }

    this_thread::sleep_for(chrono::seconds{1});

    // Now print messages!
    for(int in : this->inputPipes) {
        read(in, this->charBuffer.get(), this->BUFFER_SIZE);
        cout << fmt::format("Process ID: {} received message: {}", this->id, this->charBuffer.get()) << endl;
    }
}


inline std::string DatabaseProcess::prioQStr(std::priority_queue<Request, std::vector<Request>, RequestCompare> prioQ) {
    stringstream ss;

    ss << fmt::format("Database Process ID: {} Priority Queue!", this->id) << endl;
    int counter = 1;
    while(!prioQ.empty()) {
        DatabaseProcess::Request reqTmp = prioQ.top();
        prioQ.pop();
        ss << counter++ << ". " << reqTmp.pretty_string() << endl;
    }

    return ss.str();
}