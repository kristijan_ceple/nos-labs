//
// Created by kikyy_99 on 13.03.21.
//

#ifndef LAB1A_AUTO_H
#define LAB1A_AUTO_H


class Auto {
private:
    unsigned int id;
    Util::Way way;
    mqd_t mqId;

    Util::JsonData jsonData;
    Util::ParsedMessage parsedMessage;

    std::unique_ptr<char[]> messageBuffer;
    mq_attr mqAttr;
    size_t messageBufferSize;

//    char* messageBuffer = new char[Util::MESSAGE_BUFFER_CAPACITY];
//    char* messageBuffer = nullptr;
public:
    Auto(unsigned int ID, Util::Way way, mqd_t mqId);
    void run();
};


#endif //LAB1A_AUTO_H
