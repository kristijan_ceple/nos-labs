#include <iostream>
#include <string>
#include <fmt/format.h>
#include <fcntl.h>
#include <cerrno>
#include <thread>
#include <mqueue.h>
#include "Util.h"
#include "Auto.h"
using namespace std;

unique_ptr<Auto> me;

int main(int argc, char* argv[]) {
    if(argc != 3) {
        throw length_error("Invalid amount of arguments!");
    }

    // Arguments: car id, car Way
    unsigned int ID = stoul(argv[1]);
    Util::Way way = Util::string2Way(argv[2]);

    // Time to grab the message queue name! Then need to create the auto!
    auto mqId = mq_open(fmt::format("{}", Util::msgQueueName).c_str(), O_RDWR | O_CREAT, 0600,
                        nullptr);
    if(mqId == -1) {
        cerr << "Errno: " << errno;
        perror("Error when opening the message queue!");
        return -1;
        //throw runtime_error("Couldn't open the message queue!");
    }

    // Make me after initialising the queue
    me = make_unique<Auto>(ID, way, mqId);
    cout << fmt::format("Started Process Auto - ID: {}, Way: {}", argv[1], argv[2]) << endl;
    me->run();

    return 0;
}

//      ################################################################################################################
//      ###################################     CLASS AUTO DEFINITIONS      ############################################
//      ################################################################################################################

Auto::Auto(unsigned int ID, Util::Way way, mqd_t mqId) : id{ID}, way{way}, mqId{mqId} {
    mq_getattr(this->mqId, &this->mqAttr);
    this->messageBuffer = std::make_unique<char[]>(mqAttr.mq_msgsize);
    this->messageBufferSize = mqAttr.mq_msgsize;
}

void Auto::run() {
    // First announce our arrival
    this->jsonData = {
            .op = static_cast<int>(Util::Op::Waiting),
            .id = this->id,
            .way = static_cast<int>(this->way)
    };
    string compactMsg = JS::serializeStruct(this->jsonData, JS::SerializerOptions(JS::SerializerOptions::Compact));
    // Send to msg queue now!
    //cout << compactMsg.c_str() << endl;
    int res = mq_send(this->mqId, compactMsg.c_str(), strlen(compactMsg.c_str())+1, 0);
    if(res == -1) {
        perror("Error during message conveyance!");
        throw runtime_error("Runtime error!");
    }

    // Second - wait until we have been given the right to pass
    cout << fmt::format("Waiting - Auto - ID: {}, Way: {}", this->id, Util::way2String(this->way)) << endl;
    while(true) {
        /*
         * 1. Pick up a message
         * 2. Check if it's for me
         * 3. If it is for me: Break and proceed further, if not put the message back in the queue
         * 4. (Optional) Pause a bit
         */

        // Step 1
        res = mq_receive(this->mqId, this->messageBuffer.get(), this->messageBufferSize, nullptr);
        if(res == -1) {
            perror("Message queue error when trying to receive a message!");
            throw runtime_error("Runtime error!");
        }

        // Step2 - check if it is for me! I want to go to the bridge,
        // Read the JSON
        JS::ParseContext parseContext(this->messageBuffer.get());
        if(parseContext.parseTo(this->jsonData) != JS::Error::NoError) {
            string errorStr = parseContext.makeErrorString();
            cout << fmt::format("Error when parsing JSON 2 Struct. Error specifics: {}", errorStr) << endl;
            throw logic_error("Error during JSON Parsing!");
        }

        // Turn into a proper struct and then check
        Util::parseJson2MessageStruct(this->jsonData, this->parsedMessage);

        // Check if for me
        if (this->parsedMessage.op == Util::Op::Go && this->parsedMessage.id == this->id && this->parsedMessage.way == this->way) {
            // Go on to the bridge and pass it!
            break;
        } else {
            // Put it back into the message queue since it is not for me!
            res = mq_send(this->mqId, this->messageBuffer.get(),
                          strlen(this->messageBuffer.get())+1, 0);
            if(res == -1) {
                perror("Error during message conveyance!");
                throw runtime_error("Runtime error!");
            }
        }
    }

    // Third - pass the bridge, and upon exiting announce our exit
    // Passing the bridge is simulated by sleeping!
    cout << fmt::format("Passing the Bridge - Auto - ID: {}, Way: {}", this->id, Util::way2String(this->way))
        << endl;
    res = Util::randomCross(Util::engine);
    this_thread::sleep_for(chrono::milliseconds{res});

    // Time to announce our exit!
    this->jsonData = {
            .op = (int)Util::Op::Passed,
            .id = this->id,
            .way = (int)this->way
    };
    compactMsg = JS::serializeStruct(this->jsonData, JS::SerializerOptions(JS::SerializerOptions::Compact));

    // Send to msg queue now!
    res = mq_send(this->mqId, compactMsg.c_str(), strlen(compactMsg.c_str())+1, 0);
    if(res == -1) {
        perror("Error during message conveyance!");
        throw runtime_error("Runtime error!");
    }

    // Fourth - exit!
    cout << fmt::format("PASSED the Bridge - Auto - ID: {}, Way: {}", this->id, Util::way2String(this->way))
        << endl;
    return;
}