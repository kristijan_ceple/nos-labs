#include <iostream>
#include <fmt/format.h>
#include <fcntl.h>
#include <cerrno>
#include <thread>
#include <mqueue.h>
#include "Semaphore.h"
using namespace std;

unique_ptr<Semaphore> me;

int main() {
    // Open the message queue first
    auto mqId = mq_open(fmt::format("{}", Util::msgQueueName).c_str(), O_RDWR | O_CREAT, 0600,
                        nullptr);
    if(mqId == -1) {
        cerr << "Errno: " << errno;
        perror("Error when opening the message queue!");
        return -1;
        //throw runtime_error("Couldn't open the message queue!");
    }

    me = make_unique<Semaphore>(mqId);
    cout << "Started Process Semaphore" << endl;
    me->run();

    return 0;
}

//      ################################################################################################################
//      ###################################     CLASS SEMAPHORE DEFINITIONS      #######################################
//      ################################################################################################################
void Semaphore::run() {
    /*
     * 1. Check for requests in current Way - if 3 requests or timeout happens proceed to next Step
     * 2. If > 0 requests open the bridge for the first 3 cars waiting in current bridge Way, otherwise skip next Step
     * 3. Wait until all cars have passed
     * 4. Check if a Stop condition has been issued - if yes quit
     * 6. Change current bridge Way
     */

    // Cache
    int res;
    time_t tmpTime;

    while(true) {
        // Step 1 - checking for requests - either 3 or timeout
        auto start = chrono::steady_clock::now();
        this->abs_timeout.tv_nsec = Util::TIMEOUT_MS*1e6;
        while(this->carsQ.size() != Util::MAX_CONCURRENT_CROSSING_CARS) {
            // Check if more than X milliseconds has passed?
            auto end = chrono::steady_clock::now();
            chrono::duration<double> duration = end - start;
            if(duration.count()*1e3 > Util::TIMEOUT_MS) {
                break;      // Timeout
            }

            tmpTime = time(NULL);
            if(tmpTime == -1) {
                perror("Error during time calculation!");
                throw runtime_error("Runtime error!");
            }
            this->abs_timeout.tv_sec = tmpTime;
            this->abs_timeout.tv_nsec =  (Util::TIMEOUT_MS - duration.count() * 1e3) * 1e6;

            res = mq_timedreceive(this->mqId, this->messageBuffer.get(), this->messageBufferSize,
                                  nullptr,&this->abs_timeout);

//            cout << res << endl;
//            cout << this->messageBuffer << endl << endl;

            if(res == -1) {
                if(errno == ETIMEDOUT) {
//                    cout << "In if" << endl;
                    // ETIMEDOUT means a timeout has occurred. Go on!
                    break;
                } else {
//                    cout << "In else" << endl;
//                    cout << errno << endl;
//                    cout << strerror(errno) << endl;
                    perror("Message queue error when trying to receive a message!");
                    throw runtime_error("Runtime error!");
                }
            }

            // Parse and check if for me
            JS::ParseContext parseContext(this->messageBuffer.get());
            if(parseContext.parseTo(this->jsonData) != JS::Error::NoError) {
                string errorStr = parseContext.makeErrorString();
                cout << fmt::format("Error when parsing JSON 2 Struct. Error specifics: {}", errorStr) << endl;
                throw logic_error("Error during JSON Parsing!");
            }

            // Check if it is for me
            Util::parseJson2MessageStruct(this->jsonData, this->parsedMessage);
            if(this->parsedMessage.op == Util::Op::Waiting && this->parsedMessage.way == this->currentWay) {
                // For me!
                this->carsQ.emplace_back(this->parsedMessage.id);
            } else {
                // Not for me, put it back in the queue
                res = mq_send(this->mqId, this->messageBuffer.get(),
                              strlen(this->messageBuffer.get())+1, 0);
                if(res == -1) {
                    perror("Error during message conveyance!");
                    throw runtime_error("Runtime error!");
                }
            }
        }

        // Step 2 - check number of requests
        // Need to let those cars pass!
        if(!this->carsQ.empty()) {
            cout << "[SEMAPHORE] Letting cars pass!" << endl;
        } else {
            cout << "[SEMAPHORE] No requests present! Carrying on..." << endl;
        }

        for (unsigned int currId : this->carsQ) {
            cout << fmt::format("[SEMAPHORE] Letting car with ID: {} pass!", currId) << endl;
            // Send a message to that car!
            this->jsonData = {
                    .op = static_cast<int>(Util::Op::Go),
                    .id = currId,
                    .way = static_cast<int>(this->currentWay)
            };
            string compactMsg = JS::serializeStruct(this->jsonData, JS::SerializerOptions(JS::SerializerOptions::Compact));

            // Send to msg queue now!
            int res = mq_send(this->mqId, compactMsg.c_str(), strlen(compactMsg.c_str())+1, 0);
            if(res == -1) {
                perror("Error during message conveyance!");
                throw runtime_error("Runtime error!");
            }
        }

        // Step 3 - wait until all cars have passed
        if(!this->carsQ.empty()) {
            cout << "[SEMAPHORE] Told all cars to pass the bridge, and am now waiting for them to pass the bridge!" << endl;
        }

        int n = this->carsQ.size();
        for(int i = 0; i < n; i++) {
            // Get message into buffer
            res = mq_receive(this->mqId, this->messageBuffer.get(), this->messageBufferSize, nullptr);
            if(res == -1) {
                perror("Message queue error when trying to receive a message!");
                throw runtime_error("Runtime error!");
            }

            // Parse and check if for me
            JS::ParseContext parseContext(this->messageBuffer.get());
            if(parseContext.parseTo(this->jsonData) != JS::Error::NoError) {
                string errorStr = parseContext.makeErrorString();
                cout << fmt::format("Error when parsing JSON 2 Struct. Error specifics: {}", errorStr) << endl;
                throw logic_error("Error during JSON Parsing!");
            }


            Util::parseJson2MessageStruct(this->jsonData, this->parsedMessage);
//            if(this->jsonData.op == 2) {
//                cout << "Bingo!" << endl;
////                cout << this->jsonData << endl;
//                cout << (int)this->parsedMessage.op << endl;
//                cout << Util::op2String(this->parsedMessage.op) << endl;
//            }
            if(this->parsedMessage.op != Util::Op::Passed) {
                --i;

                // Not for me - put it back in!
                res = mq_send(this->mqId, this->messageBuffer.get(),
                              strlen(this->messageBuffer.get())+1, 0);
                if (res == -1) {
                    perror("Error during message conveyance!");
                    throw runtime_error("Runtime error!");
                }
            }
        }
        this->carsQ.clear();
        cout << "[SEMAPHORE] All cars passed - proceeding!" << endl;

        // Step 4 - check if a stop message has been issued?
        res = mq_receive(this->mqId, this->messageBuffer.get(), this->messageBufferSize, nullptr);
        if(res == -1) {
            perror("Message queue error when trying to receive a message!");
            throw runtime_error("Runtime error!");
        }

        // Parse and check if for me
        JS::ParseContext parseContext(this->messageBuffer.get());
        if(parseContext.parseTo(this->jsonData) != JS::Error::NoError) {
            string errorStr = parseContext.makeErrorString();
            cout << fmt::format("Error when parsing JSON 2 Struct. Error specifics: {}", errorStr) << endl;
            throw logic_error("Error during JSON Parsing!");
        }

        Util::parseJson2MessageStruct(this->jsonData, this->parsedMessage);
        if(this->parsedMessage.op == Util::Op::Stop) {
            cout << "[SEMAPHORE] Stop command issued! Stopping..." << endl;
            return;
        } else {
            // Not for me - put it back in!
            res = mq_send(this->mqId, this->messageBuffer.get(),
                          strlen(this->messageBuffer.get())+1, 0);
            if(res == -1) {
                perror("Error during message conveyance!");
                throw runtime_error("Runtime error!");
            }
        }

        // Step 5 - change way
        this->currentWay = this->currentWay == Util::Way::Left ? Util::Way::Right : Util::Way::Left;
        cout << fmt::format("[SEMAPHORE] Changing Way into {}!", Util::way2String(this->currentWay)) << endl;
    }
}

Semaphore::Semaphore(mqd_t mqId) : mqId{mqId} {
    mq_getattr(this->mqId, &this->mqAttr);
    this->messageBuffer = std::make_unique<char[]>(mqAttr.mq_msgsize);
    this->messageBufferSize = mqAttr.mq_msgsize;
}