//
// Created by kikyy_99 on 17.03.21.
//

#include "Util.h"
#include <iostream>
#include <fmt/format.h>
using namespace std;

std::ostream &operator<<(std::ostream &os, const Util::db_entry &dbe) {
    os << fmt::format("PID: {}, Logical Clock: {}, CS_Entries: {}", dbe.pid, dbe.logClockVal, dbe.entriesNum);
    return os;
}

void Util::checkKernelCall(int val, const string& errorMessage="Kernel call error!", bool programStart) {
    if(val == -1) {
        if((programStart && errno != ENOENT) || (!programStart && errno != EAGAIN)) {
            cerr << "Errno: " << errno << endl;
            perror(errorMessage.c_str());
            exit(-1);
        }
    }
}

std::string Util::MessageType2String(Util::MessageType toConv) {
    switch(toConv) {
        case Util::MessageType::Request:
            return "Request";
        case Util::MessageType::Answer:
            return "Answer";
        case Util::MessageType::CSExit:
            return "CSExit";
        case Util::MessageType::CSnProgramExit:
            return "CSnProgramExit";
        default:
            throw invalid_argument("String return not implemented for this MessageType! Value passed: " + to_string((int)toConv));
    }
}

Util::MessageType Util::String2MessageType(const string &toConv) {
    if(toConv == "Request") {
        return MessageType::Request;
    } else if (toConv == "Answer") {
        return MessageType::Answer;
    } else if (toConv == "CSExit") {
        return MessageType::CSExit;
    } else if (toConv == "CSnProgramExit") {
        return MessageType::CSnProgramExit;
    } else {
        throw invalid_argument("MessageType return not implemented for this String! String passed:" + toConv);
    }
}